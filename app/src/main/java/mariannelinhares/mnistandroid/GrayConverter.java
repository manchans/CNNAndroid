package mariannelinhares.mnistandroid;

import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.opengl.Matrix;
import android.util.Log;
import android.widget.ImageView;

public class GrayConverter {

    private float[][] pixels;

    public Bitmap convert(ImageView im){
        Bitmap bmp = ((BitmapDrawable) im.getDrawable()).getBitmap();
        int width = bmp.getWidth();
        int height = bmp.getHeight();
        pixels = new float[1][width*height];
        int counter = 0;
        for(int x = 0; x < width; x++) {
            for (int y = 0; y < height; y++) {
                int p = bmp.getPixel(x,y);
                int a = (p>>24)&0xff;
                int r = (p>>16)&0xff;
                int g = (p>>8)&0xff;
                int b = p&0xff;
                int avg = (r+g+b)/3;
                p = (a<<24) | (avg<<16) | (avg<<8) | avg;
                bmp.setPixel(x,y,p);
                pixels[0][counter++] = p;
            }
        }
        return bmp;
    }

    public float[] multMatrix_flatten(float b[][]){//a[m][n], b[n][p]
        float[][] a = pixels;
        if(a.length == 0) return new float[0];
        Log.d("Sahil","="+a[0].length);
        Log.d("Sahil","="+b.length);
        if(a[0].length != b.length) return null; //invalid dims

        int n = a[0].length;
        int m = a.length;
        int p = b[0].length;
        float ans[] = new float[m*p];
        int counter = 0;
        for(int i = 0;i < m;i++){
            for(int j = 0;j < p;j++){
                for(int k = 0;k < n;k++){
                    int inf = i*p+j+1;
                    //Log.d("Sahil","i*p+j+1="+ inf);
                    ans[i*p+j] += a[i][k] * b[k][j];

                }
            }
        }
        return ans;
    }

    public float[] standard_scalar(float[] input){
        float sum = 0, standardDeviation = 0;

        for(float num : input) {
            sum += num;
        }

        float mean = sum/input.length;

        for(float num: input) {
            standardDeviation += Math.pow(num - mean, 2);
        }
        standardDeviation = standardDeviation/input.length;
        standardDeviation = (float) Math.sqrt(standardDeviation);

        for(int i=0;i<input.length;i++){
            input[i] = (input[i]-mean)/standardDeviation;
        }
        return input;

    }
}
