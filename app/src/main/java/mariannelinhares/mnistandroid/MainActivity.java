package mariannelinhares.mnistandroid;

/*
   Copyright 2016 Narrative Nights Inc. All Rights Reserved.

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

   From: https://raw.githubusercontent
   .com/miyosuda/TensorFlowAndroidMNIST/master/app/src/main/java/jp/narr/tensorflowmnist
   /DrawModel.java
*/

//An activity is a single, focused thing that the user can do. Almost all activities interact with the user,
//so the Activity class takes care of creating a window for you in which you can place your UI with setContentView(View)
import android.app.Activity;
//PointF holds two float coordinates
import android.content.Context;
import android.content.Intent;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.Paint;
import android.graphics.PointF;

//A mapping from String keys to various Parcelable values (interface for data container values, parcels)
import android.graphics.drawable.BitmapDrawable;
import android.media.FaceDetector;
import android.net.Uri;
import android.os.Bundle;
//Object used to report movement (mouse, pen, finger, trackball) events.
// //Motion events may hold either absolute or relative movements and other data, depending on the type of device.
import android.provider.MediaStore;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.util.SparseArray;
import android.util.TimingLogger;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MotionEvent;
//This class represents the basic building block for user interface components.
// A View occupies a rectangular area on the screen and is responsible for drawing
import android.view.View;
//A user interface element the user can tap or click to perform an action.
import android.widget.Button;
//A user interface element that displays text to the user. To provide user-editable text, see EditText.
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
//Resizable-array implementation of the List interface. Implements all optional list operations, and permits all elements,
// including null. In addition to implementing the List interface, this class provides methods to
// //manipulate the size of the array that is used internally to store the list.
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
// basic list
import java.util.List;
import java.util.Scanner;
//encapsulates a classified image
//public interface to the classification class, exposing a name and the recognize function
import mariannelinhares.mnistandroid.models.Classification;
import mariannelinhares.mnistandroid.models.Classifier;
//contains logic for reading labels, creating classifier, and classifying
import mariannelinhares.mnistandroid.models.TensorFlowClassifier;
//class for drawing MNIST digits by finger
import mariannelinhares.mnistandroid.views.DrawModel;
//class for drawing the entire app
import mariannelinhares.mnistandroid.views.DrawView;
import Jama.SingularValueDecomposition;
import Jama.Matrix;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, View.OnTouchListener {

    public static final int PICK_IMAGE = 1;

    private static final int PIXEL_WIDTH = 40;

    // ui elements
    private Button clearBtn, classBtn, pickImageBtn;
    private Button load_15, load_25;
    private ImageView imageView;
    private TextView resText;
    private List<Classifier> mClassifiers = new ArrayList<>();

    // views
    private DrawModel drawModel;
    private DrawView drawView;
    private PointF mTmpPiont = new PointF();

    private float mLastX;
    private float mLastY;
    private float[][] covariance_mat;

    @Override
    // In the onCreate() method, you perform basic application startup logic that should happen
    //only once for the entire life of the activity.
    protected void onCreate(Bundle savedInstanceState) {
        //initialization
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //get drawing view from XML (where the finger writes the number)
        drawView = (DrawView) findViewById(R.id.draw);
        //get the model object
        drawModel = new DrawModel(PIXEL_WIDTH, PIXEL_WIDTH);

        //init the view with the model object
        drawView.setModel(drawModel);
        // give it a touch listener to activate when the user taps
        drawView.setOnTouchListener(this);

        //clear button
        //clear the drawing when the user taps
        clearBtn = (Button) findViewById(R.id.btn_clear);
        clearBtn.setOnClickListener(this);

        //class button
        //when tapped, this performs classification on the drawn image
        classBtn = (Button) findViewById(R.id.btn_class);
        classBtn.setOnClickListener(this);

        pickImageBtn = (Button) findViewById(R.id.btn_pickimg);
        pickImageBtn.setOnClickListener(this);

        imageView = (ImageView) findViewById(R.id.imageView);
        imageView.setImageResource(R.drawable.a);

        // res text
        //this is the text that shows the output of the classification
        resText = (TextView) findViewById(R.id.tfRes);
        resText.setBackgroundColor(Color.WHITE);
        load_15 = (Button) findViewById(R.id.load_15);
        load_15.setOnClickListener(this);
        load_25 = (Button) findViewById(R.id.load_25);
        load_25.setOnClickListener(this);
        Log.d("Sahil","entered");
        // tensorflow
        //load up our saved model to perform inference from local storage
        TimingLogger timings = new TimingLogger("Sahil", "methodA");
        long startTime = System.nanoTime();
        startTime = startTime/1000000000;
        loadModel();
        long mid = System.nanoTime();
        mid = mid/1000000000;
        long end = System.nanoTime();
        end = end/1000000000;
        Log.d("Sahil","" + startTime);
        Log.d("Sahil","" + mid);
        Log.d("Sahil","" + end);
        long minutes = (end - mid)/60;
        Log.d("Sahil","" + minutes);
    }

    //sahil
    private void readCovarianceMatrix(int size){
        resText.setText("Loading........." + size);
        String filename = "principal_components_" + size + ".txt";
        try {
            String each_line = null;
            int line = 0;
            int col = 0;
            String now = "";
            AssetManager assetManager = getAssets();
            BufferedReader br = new BufferedReader(new InputStreamReader(assetManager.open(filename)));
            while ((each_line = br.readLine())!=null){
                col = 0;
                Scanner sc = new Scanner(each_line);
                sc.useDelimiter(",");
                while(sc.hasNext()) {
                    now = sc.next();
                    //Log.d("Sahil","" + now);
                    covariance_mat[line][col++] = Float.parseFloat(now);

                }
                line++;
            }
            resText.setText("Loaded");
            //Log.d("Sahil","Completed" + this.covariance_mat[0][2]);
        } catch (FileNotFoundException e) {
            Log.d("Sahil", "cannot find covariance file");
        } catch (IOException e){
            Log.d("Sahil", "cannot read file");

        }
    }

    //the activity lifecycle

    @Override
    //OnResume() is called when the user resumes his Activity which he left a while ago,
    // //say he presses home button and then comes back to app, onResume() is called.
    protected void onResume() {
        drawView.onResume();
        super.onResume();
    }

    @Override
    //OnPause() is called when the user receives an event like a call or a text message,
    // //when onPause() is called the Activity may be partially or completely hidden.
    protected void onPause() {
        drawView.onPause();
        super.onPause();
    }
    //creates a model object in memory using the saved tensorflow protobuf model file
    //which contains all the learned weights
    private void loadModel() {
        //The Runnable interface is another way in which you can implement multi-threading other than extending the
        // //Thread class due to the fact that Java allows you to extend only one class. Runnable is just an interface,
        // //which provides the method run.
        // //Threads are implementations and use Runnable to call the method run().
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    //add 2 classifiers to our classifier arraylist
                    //the tensorflow classifier and the keras classifier
                    /*mClassifiers.add(
                            TensorFlowClassifier.create(getAssets(), "TensorFlow",
                                    "opt_mnist_convnet-tf.pb", "labels.txt", PIXEL_WIDTH,
                                    "input", "output", true));
                    mClassifiers.add(
                            TensorFlowClassifier.create(getAssets(), "Keras",
                                    "opt_mnist_convnet-keras.pb", "labels.txt", PIXEL_WIDTH,
                                    "conv2d_1_input", "dense_2/Softmax", false));*/
                    mClassifiers.add(
                            TensorFlowClassifier.create(getAssets(), "regression_model",
                                    "opt_regression_model.pb", "labels.txt", PIXEL_WIDTH,
                                    "dense_1_input", "dense_5/Softmax", false));
                } catch (final Exception e) {
                    //if they aren't found, throw an error!
                    throw new RuntimeException("Error initializing classifiers!", e);
                }
            }
        }).start();
    }

    @Override
    public void onClick(View view) {
        //when the user clicks something
        if (view.getId() == R.id.btn_clear) {
            //if its the clear button
            //clear the drawing
            drawModel.clear();
            drawView.reset();
            drawView.invalidate();
            //empty the text view
            resText.setText("");
        } else if (view.getId() == R.id.btn_class) {
            //if the user clicks the classify button
            //get the pixel data and store it in an array
            //float pixels[] = drawView.getPixelData();

                Bitmap bitmap = ((BitmapDrawable) imageView.getDrawable()).getBitmap();
                Bitmap converted = bitmap.copy(Bitmap.Config.RGB_565, false);
                FaceDetector.Face face= detectFaces(converted,this.getApplicationContext());
                PointF mid = new PointF();
                int x_a = ((int)mid.x);
                int y_a = ((int)mid.y);
                face.getMidPoint(mid);
                Log.d("Sahil","" + mid.x + " " + mid.y);
                Bitmap face_map = Bitmap.createBitmap(bitmap,x_a,y_a, 150,150);
            GrayConverter grayConverter = new GrayConverter();
            face_map = Bitmap.createScaledBitmap(face_map, 80, 80, false);
            imageView.setImageBitmap(face_map);
            Bitmap gray_bit = grayConverter.convert(imageView);
            float[] final_image = grayConverter.multMatrix_flatten(covariance_mat);
            Log.d("Sahil", "" + final_image.length);
            float[] in = grayConverter.standard_scalar(final_image);
            for (Classifier classifier : mClassifiers) {
                String text = "";
                final Classification res = classifier.recognize(in);
                if (res.getLabel() == null) {
                    text += classifier.name() + ": ?\n";
                } else {
                    //else output its name
                    text += String.format("%s: %s, %f\n", classifier.name(), res.getLabel(),
                            res.getConf());
                }
                resText.setText(text);

                }
            /*catch(Exception e){
                resText.setText("Exception occured");
                e.printStackTrace();
            }
*/
            /*Bitmap gray_bitmap = toGrayscale(bitmap);

            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
            byte[] pixels = baos.toByteArray();
            Log.i("Sahil", "" + pixels.length);

            Bitmap image = ((BitmapDrawable) imageView.getDrawable()).getBitmap();
            int x = image.getWidth();
            int y = image.getHeight();
            int[][] arr = new int[1][x * y];
            float[] floatArray = new float[x * y *3];
            int[] intArray  = new int[x * y];
            double[][] d_array = new double[x][y];
            float[] xxx = new float[1439];
            int counter = 0;
            for(int i=0;i<x;i++){
                for(int j=0;j<y;j++){
                    d_array[i][j] = gray_bitmap.getPixel(i,j);

                }
            }
            image.getPixels(intArray, 0, x, 0, 0, x, y);
            *//*Matrix matrix = new Matrix(d_array);
            SingularValueDecomposition s = matrix.svd();
            Log.d("Sahil","in " + s.getSingularValues().length);
            double[] r = s.getSingularValues();
            for(int i=0;i<1439;i++){
                    xxx[i] = (float)r[i];
            }*//*

            *//*for(int i = 0; i < intArray.length; i++) {
                floatArray[i] = (float) Color.red(intArray[i]); //Any colour will do
            }*//*
            for (int i = 0; i < intArray.length; ++i) {
                final int val = intArray[i];
                floatArray[i * 3 + 0] = Color.red(intArray[i]);
                floatArray[i * 3 + 1] = Color.green(intArray[i]);
                floatArray[i * 3 + 2] = Color.blue(intArray[i]);
            }
            //init an empty string to fill with the classification output
            String text = "";
            //for each classifier in our array
            for (Classifier classifier : mClassifiers) {
                //perform classification on the image
                final Classification res = classifier.recognize(floatArray);
                //if it can't classify, output a question mark
                if (res.getLabel() == null) {
                    text += classifier.name() + ": ?\n";
                } else {
                    //else output its name
                    text += String.format("%s: %s, %f\n", classifier.name(), res.getLabel(),
                            res.getConf());
                }
            }
            resText.setText(text);*/
        }
        else if(view.getId() == R.id.btn_pickimg){
            Intent intent = new Intent(Intent.ACTION_PICK,
                    MediaStore.Images.Media.INTERNAL_CONTENT_URI);
            intent.setType("image/*");
            intent.putExtra("crop", "true");
            intent.putExtra("scale", true);
/*            intent.putExtra("outputX", 1000);
            intent.putExtra("outputY", 1000);*/
            intent.putExtra("aspectX", 1);
            intent.putExtra("aspectY", 1);
            intent.putExtra("return-data", true);
            startActivityForResult(intent, 1);
        }
        else if(view.getId() == R.id.load_15){
            Log.d("Sahil","Loading");
            resText.setText("Loading....15");
            covariance_mat = new float[6400][15*15];
            long startTime = System.nanoTime();
            startTime = startTime/1000000000;
            long mid = System.nanoTime();
            mid = mid/1000000000;
            readCovarianceMatrix(15);
            long end = System.nanoTime();
            end = end/1000000000;
            Log.d("Sahil","" + startTime);
            Log.d("Sahil","" + mid);
            Log.d("Sahil","" + end);
            long minutes = (end - mid)/60;
            Log.d("Sahil","" + minutes);
            Toast.makeText(this,"loaded in " + minutes,Toast.LENGTH_LONG).show();

        }
        else if(view.getId() == R.id.load_25){
            resText.setText("Loading....25");
            covariance_mat = new float[6400][625];
            long startTime = System.nanoTime();
            startTime = startTime/1000000000;
            long mid = System.nanoTime();
            mid = mid/1000000000;
            readCovarianceMatrix(25);
            long end = System.nanoTime();
            end = end/1000000000;
            Log.d("Sahil","" + startTime);
            Log.d("Sahil","" + mid);
            Log.d("Sahil","" + end);
            long minutes = (end - mid)/60;
            Log.d("Sahil","" + minutes);
            Toast.makeText(this,"loaded in " + minutes,Toast.LENGTH_LONG).show();
        }
    }

    @Override
    //this method detects which direction a user is moving
    //their finger and draws a line accordingly in that
    //direction
    public boolean onTouch(View v, MotionEvent event) {
        //get the action and store it as an int
        int action = event.getAction() & MotionEvent.ACTION_MASK;
        //actions have predefined ints, lets match
        //to detect, if the user has touched, which direction the users finger is
        //moving, and if they've stopped moving

        //if touched
        if (action == MotionEvent.ACTION_DOWN) {
            //begin drawing line
            processTouchDown(event);
            return true;
            //draw line in every direction the user moves
        } else if (action == MotionEvent.ACTION_MOVE) {
            processTouchMove(event);
            return true;
            //if finger is lifted, stop drawing
        } else if (action == MotionEvent.ACTION_UP) {
            processTouchUp();
            return true;
        }
        return false;
    }

    //draw line down

    private void processTouchDown(MotionEvent event) {
        //calculate the x, y coordinates where the user has touched
        mLastX = event.getX();
        mLastY = event.getY();
        //user them to calcualte the position
        drawView.calcPos(mLastX, mLastY, mTmpPiont);
        //store them in memory to draw a line between the
        //difference in positions
        float lastConvX = mTmpPiont.x;
        float lastConvY = mTmpPiont.y;
        //and begin the line drawing
        drawModel.startLine(lastConvX, lastConvY);
    }

    //the main drawing function
    //it actually stores all the drawing positions
    //into the drawmodel object
    //we actually render the drawing from that object
    //in the drawrenderer class
    private void processTouchMove(MotionEvent event) {
        float x = event.getX();
        float y = event.getY();

        drawView.calcPos(x, y, mTmpPiont);
        float newConvX = mTmpPiont.x;
        float newConvY = mTmpPiont.y;
        drawModel.addLineElem(newConvX, newConvY);

        mLastX = x;
        mLastY = y;
        drawView.invalidate();
    }

    private void processTouchUp() {
        drawModel.endLine();
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        if (requestCode == PICK_IMAGE && data != null) {
            Uri selectedImage = data.getData();
            Bundle extras = data.getExtras();
            if (extras != null) {
                //Get image
                Bitmap newProfilePic = extras.getParcelable("data");
                imageView.setImageBitmap(newProfilePic);

            }

        }
    }
    public static Bitmap toGrayscale(Bitmap bmpOriginal)
    {
        int width, height;
        height = bmpOriginal.getHeight();
        width = bmpOriginal.getWidth();

        Bitmap bmpGrayscale = Bitmap.createBitmap(width, height, Bitmap.Config.RGB_565);
        Canvas c = new Canvas(bmpGrayscale);
        Paint paint = new Paint();
        ColorMatrix cm = new ColorMatrix();
        cm.setSaturation(0);
        ColorMatrixColorFilter f = new ColorMatrixColorFilter(cm);
        paint.setColorFilter(f);
        c.drawBitmap(bmpOriginal, 0, 0, paint);
        return bmpGrayscale;
    }
    private static FaceDetector.Face detectFaces(Bitmap image , Context context) {

        int h = image.getHeight();
        int w = image.getWidth();
        FaceDetector fd = new FaceDetector(w,h,1);
        FaceDetector.Face[] faces = new FaceDetector.Face[1];
        int result = fd.findFaces(image,faces);
        return faces[0];





    }
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.my_options_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }
}